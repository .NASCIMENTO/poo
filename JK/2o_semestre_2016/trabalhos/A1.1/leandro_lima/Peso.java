// Programa para calculo do IMC
//Autor:Leandro do Nascimento Lima

import javax.swing.JOptionPane;

/**
 * Calculo do IMC em java IMC = pesoEmQuilogramas / (alturaEmMetros *
 * alturaEmMetros)
 * 
 */
//classe principal
public class Peso {

	// m�todo principal
	public static void main(String[] args) {

		// adiquirindo dados digitados pelo usuário
		String peso = JOptionPane.showInputDialog("Digite seu peso em Quilogramas: ");
		String altura = JOptionPane.showInputDialog("Digite sua altura em metros: ");
		
		//transformando Strings em double
		double pesoEmQuilogramas = Double.parseDouble(peso);
		double alturaEmMetros = 1.80;
		double imc = pesoEmQuilogramas / (alturaEmMetros * alturaEmMetros);

		// F�rmula para calculo do imc
		String msg = (imc >= 20 && imc <= 25) ? "Peso Ideal" : "Fora do Peso Ideal.";
		msg = "IMC = " + imc + "\n" + msg;

		// impress�o usando m�todo JOptionPane
		JOptionPane.showMessageDialog(null, msg);
		System.out.println("IMC = " + imc);
		System.out.println(msg);

	}
}
